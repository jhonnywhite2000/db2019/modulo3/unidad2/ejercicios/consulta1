<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function  actionCrud(){
        return $this->render("gestion");
    }
    public function actionConsulta1() {
      // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand('SELECT count(distinct edad) FROM ciclista')
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
                'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'], 
            "enunciado"=>"Listar las edad de los ciclistas sin repetidos",
             "titulo"=>"Consulta1a",
            "sql"=>"Select distinct from ciclista",
        ]);
        
        
    }
    
    
    
    public function  actionConsulta1a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->distinct()->select("edad"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'], 
            "enunciado"=>"Listar las edad de los ciclistas sin repetidos",
             "titulo"=>"Consulta1a",
            "sql"=>"Select distinct from ciclista",
        ]);
    }
    public function  actionConsulta2(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'artiach'")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "artiach"',
            'totalCount'=>$numero,
                'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'], 
            "enunciado"=>"Listar las edad de los ciclistas de Artiach",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT DISTINCT ciclista.edad FROM ciclista WHERE ciclista.nomequipo = 'artiach';",
        ]);
    }
    public function  actionConsulta2a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("edad")
                ->where("nomequipo = 'artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'], 
            "enunciado"=>"Listar las edad de los ciclistas de Artiach",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT DISTINCT ciclista.edad FROM ciclista WHERE ciclista.nomequipo = 'artiach';",
        ]);
    }
    
    public function  actionConsulta3a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("edad")
                ->where("nomequipo = 'artiach' OR nomequipo = 'Amore Vita' "),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'], 
            "enunciado"=>"Listar las edad de los ciclistas de Artiach",
             "titulo"=>"Consulta 3",
            "sql"=>"SELECT DISTINCT ciclista.edad FROM ciclista WHERE ciclista.nomequipo = 'artiach' OR ciclista.nomequipo = 'Amore Vita';",
        ]);
    }
    public function  actionConsulta3(){
       // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'artiach' OR nomequipo = 'Amore Vita'")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "artiach" OR nomequipo = "Amore Vita"',
            'totalCount'=>$numero,
                'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'], 
            "enunciado"=>"Listar las edad de los ciclistas de Artiach o Amore Vita",
             "titulo"=>"Consulta 3",
            "sql"=>"SELECT DISTINCT ciclista.edad FROM ciclista WHERE ciclista.nomequipo = 'artiach'  OR ciclista.nomequipo = 'Amore Vita';",
        ]);
    }
     public function  actionConsulta4a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("dorsal")
                ->where("edad < 25 OR edad > 30 "),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar los dorsales de los ciclistas de edad < 25 or > 30",
             "titulo"=>"Consulta 3",
            "sql"=>"SELECT ciclista.dorsal FROM ciclista WHERE  edad < 25 or > 30;",
        ]);
    }
    
    public function  actionConsulta4(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT count(dorsal) FROM ciclista WHERE edad < 25 OR edad > 30")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
            'totalCount'=>$numero,
                'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar los dorsales de los ciclistas de edad < 25 O > 30",
             "titulo"=>"Consulta 3",
            "sql"=>"SELECT ciclista.dorsal FROM ciclista WHERE edad < 25 OR edad > 30;",
        ]);
    }
    
    public function  actionConsulta5a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("dorsal")
                ->where("nomequipo = 'banesto'and edad BETWEEN 28 AND 32"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar los dorsales de los ciclistas de edad  entre 28 y 32 años",
             "titulo"=>"Consulta 5",
            "sql"=>"SELECT ciclista.dorsal FROM ciclista WHERE nomequipo = 'banesto'and edad BETWEEN 28 AND 32 ;",
        ]);
    }
    public function  actionConsulta5(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT count(dorsal) FROM ciclista WHERE nomequipo = 'banesto'and edad BETWEEN 28 AND 32")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM ciclista WHERE nomequipo = "banesto" and edad BETWEEN 28 AND 32',
            'totalCount'=>$numero,
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar los dorsales de los ciclistas de edad  entre 28 y 32 años",
             "titulo"=>"Consulta 5",
            "sql"=>"SELECT ciclista.dorsal FROM ciclista WHERE nomequipo = 'banesto'and edad BETWEEN 28 AND 32;",
        ]);
    }
    public function  actionConsulta6a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("nombre")
                ->where("CHAR_LENGTH(nombre) > 8"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'], 
            "enunciado"=>"Listar los ciclistas cuyo nombe tenga mas de ocho caracteres",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT nombre, CHAR_LENGTH(nombre) FROM ciclista WHERE  CHAR_LENGTH(nombre) > 8;",
        ]);
    }
    public function  actionConsulta6(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) FROM ciclista WHERE  CHAR_LENGTH(nombre) > 8")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre FROM ciclista WHERE  CHAR_LENGTH(nombre) > 8',
            'totalCount'=>$numero,
            
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'], 
            "enunciado"=>"Listar los ciclistas cuyo nombe tenga mas de ocho caracteres",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT nombre FROM ciclista WHERE  CHAR_LENGTH(nombre) > 8;",
        ]);
    }
    public function  actionConsulta7a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("upper(nombre)nombre"),
            'pagination'=>[
                'pageSize'=>20,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'], 
            "enunciado"=>"Listar el nombre y los dorsales de los ciclistas con un campo nuevo con los nombres en mayuscula",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT nombre, dorsal, UCASE(nombre) AS nombremayuscula FROM ciclista;",
        ]);
    }
      public function  actionConsulta7(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) FROM ciclista ")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT upper(nombre) nombre FROM ciclista',
            'totalCount'=>$numero,
            
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'], 
            "enunciado"=>"Listar el nombre y los dorsales de los ciclistas con un campo nuevo con los nombres en mayuscula",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT nombre, dorsal, UCASE(nombre) AS nombremayuscula FROM ciclista;",
        ]);
    }
    public function  actionConsulta8a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()
                ->distinct()
                ->select("dorsal")
                ->where("código='MGE'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar todos los ciclistas  que han llevado maillot MGE(amarillo)en alguna etapa etapas",
             "titulo"=>"Consulta 8",
            "sql"=>"SELECT  DISTINCT dorsal FROM lleva WHERE código='MGE';",
        ]);
    }
    public function  actionConsulta8(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT count(DISTINCT dorsal) FROM lleva WHERE código='MGE'")
              ->queryScalar();
       
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM lleva WHERE  código="MGE"',
            'totalCount'=>$numero,
            
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar todos los ciclistas  que han llevado maillot MGE(amarillo)en alguna etapa etapas",
             "titulo"=>"Consulta 8",
            "sql"=>"SELECT  DISTINCT dorsal FROM lleva WHERE código='MGE';",
        ]);
    }    
   
    public function  actionConsulta9a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => puerto::find()
                ->distinct()
                ->select("nompuerto")
                ->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'], 
            "enunciado"=>"Listar el  nombre de los puertos cuya altura es mayor que 1500",
             "titulo"=>"Consulta 9",
            "sql"=>"select nompuerto from puerto WHERE altura>1500;",
        ]);
    }
    public function  actionConsulta9(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("select count(*) from puerto WHERE altura>1500")
              ->queryScalar();
       
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nompuerto from puerto WHERE altura>1500',
            'totalCount'=>$numero,
            
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'], 
            "enunciado"=>"Listar el  nombre de los puertos cuya altura es mayor que 1500",
             "titulo"=>"Consulta 9",
            "sql"=>"select nompuerto from puerto WHERE altura>1500;",
        ]);
    }  
    
    public function  actionConsulta10a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => puerto::find()
                ->distinct()
                ->select("dorsal")
                ->where("pendiente > 8 OR altura BETWEEN 1800 AND 3000"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hallan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000",
             "titulo"=>"Consulta 10",
            "sql"=>"select dorsal from puerto WHERE pendiente > 8  OR altura BETWEEN 1800 AND 3000;",
        ]);
    }
    public function  actionConsulta10(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("select count(*) from puerto WHERE pendiente > 8  OR altura BETWEEN 1800 AND 3000")
              ->queryScalar();
       
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from puerto WHERE pendiente > 8  OR altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hallan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000",
             "titulo"=>"Consulta 10",
            "sql"=>"select dorsal from puerto WHERE pendiente > 8  OR altura BETWEEN 1800 AND 3000;",
        ]);
    }  
    
   public function  actionConsulta11a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => puerto::find()
                ->distinct()
                ->select("dorsal")
                ->where("pendiente > 8 AND altura BETWEEN 1800 AND 3000"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hallan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000",
             "titulo"=>"Consulta 11",
            "sql"=>"select dorsal from puerto WHERE pendiente > 8  AND altura BETWEEN 1800 AND 3000;",
        ]);
    }
    public function  actionConsulta11(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("select count(*) from puerto WHERE pendiente > 8  AND altura BETWEEN 1800 AND 3000")
              ->queryScalar();
       
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from puerto WHERE pendiente > 8  AND altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            
                'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hallan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000",
             "titulo"=>"Consulta 11",
            "sql"=>"select dorsal from puerto WHERE pendiente > 8  AND altura BETWEEN 1800 AND 3000;",
        ]);
    } 
}
