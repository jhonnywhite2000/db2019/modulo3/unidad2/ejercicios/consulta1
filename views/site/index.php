<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección!</h1>
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">
         <div class="row">
             <!--
             Boton de consulta
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 1</h3>
                    <p>Listar la edad de los ciclistas </p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta1a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <!--
             Boton de consulta dos
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 2</h3>
                    <p>Listar lasedad de los ciclistas de Artiach </p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta2a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             
             <!--
             Boton de consulta tres
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 3</h3>
                    <p>Listar la edad de los ciclistas de Artiach o Amore Vita</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta3a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div>   
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 4</h3>
                    <p>Listar los dorsales de los ciclistas  cuya edad sea menor que 25 o  mayor que 30 </p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta4a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
               <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 5</h3>
                    <p>Listar los dorsales de los ciclistas  cuya edad este entree  28 y 32 y ademas sean de Banesto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta5a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 6</h3>
                    <p>Indicar el nombre el numero de los ciclistas  sea mayor que ocho(8) </p>
                    <?= Html::a('Active record', ['site/consulta6a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 7</h3>
                    <p>Listar el nombre y los dorsales de los ciclistas con un campo nuevo con los nombres en mayuscula</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta7a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                    <p>Listar todos los ciclistas  que han llevado maillot MGE(amarillo)en alguna etapa etapas</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta8a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                    <p>Listar el  nombre de los puertos cuya altura es mayor que 1500</p>
                    <?= Html::a('Active record', ['site/consulta9a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                    <p>Listar el dorsal de los ciclistas que hallan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta10a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 11</h3>
                    <p>Listar el dorsal de los ciclistas que hallan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta11a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div> 
    </div>
</div>
